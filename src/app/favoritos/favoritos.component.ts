import { Component, OnInit } from "@angular/core";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { NoticiasService } from "../domain/noticias.service";
import { Noticia, NoticiasLeidasAction } from "../domain/noticias-state.model";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

import * as Toast from "nativescript-toasts";

@Component({
    selector: "Favoritos",
    templateUrl: "./favoritos.component.html"
})
export class FavoritosComponent implements OnInit  {

    favoritos: Array<string> = [];

    constructor(private noticiasService: NoticiasService, private store: Store<AppState>) {}

    ngOnInit(): void {
        this.noticiasService.favs()
            .then((f: any) => {
                console.log("favoritos: " + JSON.stringify(f));
                this.favoritos = f;
            }, (e) => {
                console.log("error cargando favoritos " + e);
                Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT}); 
            });
    }

    leerAhora(noticia: string) {
        this.store.dispatch(new NoticiasLeidasAction(new Noticia(noticia)));
    }


    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}