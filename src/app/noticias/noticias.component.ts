import { Component, Input, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "Noticias",
    templateUrl: "./noticias.component.html"
})
export class NoticiasComponent {

    title: string;

    constructor(private router: ActivatedRoute) {
        router.queryParams.subscribe(queries => this.title = queries['noticia']);
        console.log(this.title);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }


}