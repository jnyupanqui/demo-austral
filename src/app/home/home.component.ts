import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

import * as Toast from "nativescript-toasts";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { Noticia } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";

import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    leidos: Array<Noticia> = [];

    constructor(private store: Store<AppState>) {
    }

    onButtonTap(): void {
        camera.requestPermissions().then(
            function success() {
                const options = {width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options)
                    .then((imageAsset) => {
                        console.log("Tamano: " + imageAsset.options.width + "x" + imageAsset.options.height);
                        console.log("KeepAspectRatio: " + imageAsset.options.keepAspectRatio);
                        console.log("Foto guardada!");
                        const image = new Image();
                        image.src = imageAsset;
                    }).catch((err) => {
                        console.log("Error -> " + err.message)
                    });
            },
            function failure() {
                console.log("Permiso de camera no aceptado por el usuario");
            }
        );
    }

    doLater(fn) {
        setTimeout(fn, 1000);
    }

    ngOnInit(): void {
        this.store.select(state => state.noticias.leidas)
            .subscribe(data => {
                if(data != null) {
                    this.leidos = data;
                }
            });

        const toastOption: Toast.ToastOptions = { text: "Hello world", duration: Toast.DURATION.SHORT };
        this.doLater(() => Toast.show(toastOption));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
