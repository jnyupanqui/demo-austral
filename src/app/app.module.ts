import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasService } from "./domain/noticias.service";
import { NoticiasComponent } from "./noticias/noticias.component";
import { NoticiasState, reducersNoticias, initNoticiasState, NoticiasEffects } from "./domain/noticias-state.model";

import { ActionReducerMap, StoreModule as NGRXStoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

// redux init
export interface AppState {
    noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducersNoticias
}

const reducersInitialState = {
    noticias: initNoticiasState()
}
// fin redux

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NGRXStoreModule.forRoot(reducers, { initialState: reducersInitialState, runtimeChecks: {
            strictStateImmutability: false,
            strictActionImmutability: false
        } }),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    declarations: [
        AppComponent,
        NoticiasComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [
        NoticiasService
    ]
})
export class AppModule { }
