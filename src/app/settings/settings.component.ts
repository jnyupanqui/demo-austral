import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

import * as dialogs from "tns-core-modules/ui/dialogs";

import * as appSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    private nombreUsuario: string = "";

    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    doLater(fn) {
        setTimeout(fn, 1000);
    }

    ngOnInit(): void {
        if(!appSettings.hasKey("nombreUsuario")) {
            this.nombreUsuario = "vacio";
        } else {
            this.nombreUsuario = appSettings.getString("nombreUsuario");
        }
        

        this.doLater(() => {
            dialogs.action("Mensaje", "Cancelar", ["Opcion 1", "Opcion 2"])
            .then((result) => {
                console.log('resultado: ' + result);
                if(result==="Opcion 1") {
                    this.doLater(() => dialogs.alert({
                        title: "Title 1",
                        message: "msg 1",
                        okButtonText: "bn 1"
                    }).then(() => console.log("Cerrando 1")));
                }
                else if(result==="Opcion 2") {
                    this.doLater(() => dialogs.alert({
                        title: "Title 2",
                        message: "msg 2",
                        okButtonText: "bn 2"
                    }).then(() => console.log("Cerrando 2")));

                }
            });
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });
    }
}
