import { Component } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import * as app from "tns-core-modules/application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

@Component({
    selector: "Edicion",
    templateUrl: "./edicion.component.html"
})
export class EdicionComponent {

    nombreUsuario: string = "";


    onButtonTap() {
        if(this.nombreUsuario.trim().length > 0) {
            appSettings.setString("nombreUsuario", this.nombreUsuario);
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

}