import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";

import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";


@Component({
    selector: "SearchForm",
    moduleId: module.id,
    template: `
    <StackLayout>
    <TextField #texto="ngModel" [(ngModel)]="textFieldValue" required minlen="4"></TextField>
    <Label *ngIf="texto.hasError('required')" text="input empty"></Label>
    <Label *ngIf="!texto.hasError('required') && !texto.hasError('minlen')" text=">4"></Label>
    <Button text="Buscar" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
    
    <Button text="Busy Toggle" (tap)="(activityIndicator.busy=!activityIndicator.busy)" 
    class="btn btn-primary btn-active"></Button>
    <ActivityIndicator #activityIndicator busy="true" (busyChange)="cambio($event)"
     width="100" height="100" class="activity-indicator"></ActivityIndicator>
    </StackLayout>
    `
})
export class SearchForm {

    @Output()
    searchEvent: EventEmitter<string>;

    textFieldValue: string = "noticia";

    constructor() {
        this.searchEvent = new EventEmitter();
    }

    cambio(e) {
        let indicator = <ActivityIndicator>e.object;
        console.log("indicator.busy: " + indicator.busy);
    }

    onButtonTap() {
        if(this.textFieldValue.length > 2) {
            this.searchEvent.emit(this.textFieldValue);
        }
    }


}