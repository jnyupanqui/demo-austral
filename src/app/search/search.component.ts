import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";

import { View, Color } from "tns-core-modules/ui/core/view/view";
import { RouterExtensions } from "nativescript-angular/router";

import * as Toast from "nativescript-toasts";

import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { NuevaNoticiaAction, Noticia } from "../domain/noticias-state.model";

import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {

    resultados: Array<string> = [];

    @ViewChild("layout", { static: true }) layout: ElementRef;

    constructor(private store: Store<AppState>,
        public notiasService: NoticiasService, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string) {
        console.log("===> " + s);
        this.notiasService.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT}); 
        });

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 3000,
            delay: 1500
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 3000,
            delay: 1500
        }));
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((noticia: Noticia) => {
            if(noticia != null) {
                Toast.show({text: "Sugerimos leer: " + noticia.titulo, duration: Toast.DURATION.SHORT});
            }
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    refreshList(args) {
        const pullRefresh = args.object;
        setTimeout(function () {
            pullRefresh.refreshing = false;
            console.log("==> refresh");
        }, 1000);
    }

    onNavItemTap(navItemRoute: string, noticia: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            },
            queryParams: {
                noticia: noticia
            },
            queryParamsHandling: "merge"
        });
    }
}
