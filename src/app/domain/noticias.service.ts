import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http"

const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {

    api: string = "https://315c0eb47853.ngrok.io";

    constructor() {
        this.getDb((db) => {
            console.log(db);
            db.each("select * from logs", // begin one time
                (err, fila) => console.log("fila: ", fila), // iterable
                (err, totales) => console.log("Filas totales: ", totales)); //after one time
        }, () => console.log("error on getDb"));
    }

    getDb(fnOk, fnError) {
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir", err);
            } else {
                console.log("Esta la db abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    agregar(s: string) {
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {
        this.getDb((db) => {
            db.execSQL("insert into logs (texto) values (?)", [s],
                (err, id) => console.log("numero id: ", id));
        }, () => console.log("error on getDb"));

        return getJSON(this.api + "/get?q=" + s);
    }
}