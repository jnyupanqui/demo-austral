import { Action } from "@ngrx/store";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType} from "@ngrx/effects";
import { map } from "rxjs/operators";

export class Noticia {
    constructor(public titulo: string) {}
}

export interface NoticiasState {
    items: Noticia[];
    sugerida: Noticia;
    leidas: Noticia[];
}

export function initNoticiasState() {
    return {
        items: [],
        sugerida: null,
        leidas: []
    };
}

export enum NoticiasActionTypes {
    INIT_MY_DATA = "[Noticias] Init My Data",
    NUEVA_NOTICIA = "[Noticias] Nueva",
    SUGERIR_NOTICIA = "[Noticias] Sugerir",
    LEIDAS = "[Noticias] Noticias Leidas"
}

export class InitMyDataAction implements Action {
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public titulares: Array<string>) {}
}

export class NuevaNoticiaAction implements Action {
    type = NoticiasActionTypes.NUEVA_NOTICIA;
    constructor(public noticia: Noticia) {}
}

export class SugerirNoticiaAction implements Action {
    type = NoticiasActionTypes.SUGERIR_NOTICIA;
    constructor(public noticia: Noticia) {}
}

export class NoticiasLeidasAction implements Action {
    type = NoticiasActionTypes.LEIDAS;
    constructor(public noticia: Noticia) {}
}

export type NoticiasViajesActions = InitMyDataAction | NuevaNoticiaAction | SugerirNoticiaAction | NoticiasLeidasAction;

export function reducersNoticias(
    state: NoticiasState,
    action: NoticiasViajesActions
): NoticiasState {
    switch(action.type) {
        case NoticiasActionTypes.INIT_MY_DATA: {
            const titulares: Array<string> = (action as InitMyDataAction).titulares;

            return {
                ...state,
                items: titulares.map((t) => new Noticia(t))
            };
        }
        case NoticiasActionTypes.NUEVA_NOTICIA: {
            return {
                ...state,
                items: [...state.items, (action as NuevaNoticiaAction).noticia]
            };
        }
        case NoticiasActionTypes.SUGERIR_NOTICIA: {
            return {
                ...state,
                sugerida: (action as SugerirNoticiaAction).noticia
            };
        }
        case NoticiasActionTypes.LEIDAS: {
            return {
                ...state,
                leidas: [...state.leidas, (action as NoticiasLeidasAction).noticia]
            };
        }
    }

    return state;
}

@Injectable()
export class NoticiasEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(NoticiasActionTypes.NUEVA_NOTICIA),
        map((action: NuevaNoticiaAction) => new SugerirNoticiaAction(action.noticia))
    );

    constructor(private actions$: Actions) {}
}